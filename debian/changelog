python-mongoengine (0.29.1-1) unstable; urgency=high

  * Team upload
  * New upstream release
  * Fix autopkgtest
  * Require the new pymongo

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Tue, 15 Oct 2024 01:01:58 +0200

python-mongoengine (0.28.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.28.2
  * Drop upstream merged patch

 -- Ananthu C V <weepingclown@disroot.org>  Tue, 12 Mar 2024 00:29:40 +0530

python-mongoengine (0.27.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.27.0.
  * Add patch file 0002-add-missing-doc-link to add
    missing reference link to documentation.
  * Bump Standards-Version to 4.6.2 (no changes needed).

 -- Ananthu C V <weepingclown@disroot.org>  Sat, 30 Dec 2023 15:59:12 +0000

python-mongoengine (0.24.2-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 01 Dec 2022 11:34:43 +0000

python-mongoengine (0.24.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-mongoengine-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 12:28:29 +0000

python-mongoengine (0.24.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + python3-mongoengine: Drop versioned constraint on python3-pymongo in
      Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 02:53:58 +0100

python-mongoengine (0.24.2-1) unstable; urgency=medium

  * New upstream release.
  * Drop use of ${python3:Provides}
  * autopkgtest:
    - Replace superficial autopkgtest with autodep8 configuration.
    - Rewrite autopkgtest so we test for all supported versions.
  * Update maintainer name and email.
  * d/copyright: Include myself in Debian paragraph.
  * Update Standards-Version to 4.6.1

 -- Håvard F. Aasen <havard.f.aasen@pfft.no>  Sun, 07 Aug 2022 07:34:11 +0200

python-mongoengine (0.23.1-1) unstable; urgency=medium

  * New upstream version 0.23.1
  * Drop old patch, no longer needed.
  * Add patch to disable sphinx extension.
  * d/gbp.conf: Added new file.
  * d/control: Update Standards-Version to 4.6.0

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sat, 11 Sep 2021 23:12:42 +0200

python-mongoengine (0.21.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Håvard Flaget Aasen ]
  * New upstream version 0.21.0
  * d/watch:
    - Update to version 4
    - Use the new substitution string from uscan.
  * d/control:
    - Update Standards-Version to 4.5.1
    - Drop version restriction on suggested packages.
  * d/rules: Change target to "execute_after"
  * autopkgtest: Mark tests as superficial.

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Thu, 24 Dec 2020 15:38:08 -0500

python-mongoengine (0.20.0-1) unstable; urgency=medium

  * New upstream version 0.20.0
  * Bump debhelper to 13
  * Rebase patch
  * Add python3-blinker as suggested package
  * Fix Source URI in d/copyright

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Wed, 13 May 2020 09:18:27 +0200

python-mongoengine (0.19.1-1) unstable; urgency=medium

  * New upstream version 0.19.1
  * Update Standards-Version to 4.5.0
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse
  * Add 0001-Loosen-version-control-on-python3-pil.patch
  * Add autopkgtest

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Fri, 31 Jan 2020 10:35:36 +0100

python-mongoengine (0.19.0-1) unstable; urgency=medium

  * New upstream version 0.19.0
  * d/control
    - Add python3-dateutil and python3-pil to suggests
    - Add Rules-Requires-Root: no
  * Add CI in salsa

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Thu, 02 Jan 2020 11:30:49 +0100

python-mongoengine (0.18.2-1) unstable; urgency=medium

  * Team upload

  * New upstream version 0.18.2 Closes: 919456
  * Bump dh to 12
  * Update Standards-Version to 4.4.1
  * Remove entry for sphinx-rtd-theme in d/copyright
  * Disable tests since these is not supplied through PyPi
  * Delete d/pydist-overrides since this no longer applies.
  * Remove some build dependencies.
  * Add doc package as suggested in python3 package.
  * Add myself as uploader. Closes: 849736

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Mon, 09 Dec 2019 13:10:40 +0100

python-mongoengine (0.15.3-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Add ${sphinxdoc:Depends} to python-mongoengine-doc (Closes: #919845).
  * Drop Python 2 support.

 -- Andrey Rahmatullin <wrar@debian.org>  Sat, 10 Aug 2019 11:32:37 +0500

python-mongoengine (0.15.3-1) unstable; urgency=medium

  * Team upload.

  [ Orestis Ioannou ]
  * Drop python-django from recommends since mongoengine no longer
  * supports django. A specific extension for django has been created.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

  [ Christoph Berg ]
  * New upstream version. (Closes: #906402)
  * Remove sphinx javascript hacks.
  * Set PYTHONPATH while building docs.
  * Remove docs/_build/ on clean.
  * Depend on python3 in the python3 module. (Closes: #867442)
  * Set team as maintainer, thanks Janos!
    (We don't have a new uploader yet, leave #849736 open.)

 -- Christoph Berg <myon@debian.org>  Sat, 29 Sep 2018 16:35:09 +0200

python-mongoengine (0.10.6-1) unstable; urgency=low

  * Team upload.
  * Svn to git migration.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Remove DM-Upload-Allowed; it's no longer used by the archive
    software.

  [ Orestis Ioannou ]
  * New upstream release (Closes: #738503, #789980).
  * Use pybuild to build package.
  * Provide Python 3 package.
  * Move compat version to 9.
  * Pump up standards version to 3.9.7. Nothing to change.
  * Run wrap-and-sort.
  * Add dependencies for running tests.
  * Use pypi.debian.net for d/watch.
  * Added a patch to use libjs-modernizr instead of downloading from cdn.
  * Switch Vcs-git field to https.

 -- Orestis Ioannou <orestis@oioannou.com>  Wed, 09 Mar 2016 01:39:25 +0100

python-mongoengine (0.6.13-2) unstable; urgency=low

  * Preserve _sources from html documentation to make sphinx search work.

 -- Janos Guljas <janos@resenje.org>  Wed, 27 Jun 2012 23:15:17 +0200

python-mongoengine (0.6.13-1) unstable; urgency=low

  * New upstream release.

 -- Janos Guljas <janos@resenje.org>  Sun, 24 Jun 2012 21:25:30 +0200

python-mongoengine (0.6.12-1) unstable; urgency=low

  * New upstream release.
  * debian/watch
    - Update source location.
  * debian/copyright
    - Update source location.

 -- Janos Guljas <janos@resenje.org>  Thu, 21 Jun 2012 18:59:24 +0200

python-mongoengine (0.6.8-1) unstable; urgency=low

  * New upstream release.
  * debian/source/options
    - Ignore mongoengine.egg-info changes after build. (Closes: #671422)
  * debian/rules
    - Remove unused variable PREFIX.
    - Remove relocation of 'tests' directory.
  * Add exclude-tests-from-packaging.patch to exclude 'tests' from packaging
    it in global namespace.

 -- Janos Guljas <janos@resenje.org>  Wed, 16 May 2012 19:40:07 +0200

python-mongoengine (0.6.4-1) unstable; urgency=low

  * New upstream release.

 -- Janos Guljas <janos@resenje.org>  Sun, 22 Apr 2012 23:02:56 +0200

python-mongoengine (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * Update version regular expression in debian/watch.
  * Bump standards to 3.9.3.
  * Update debian/copyright Format value.

 -- Janos Guljas <janos@resenje.org>  Tue, 06 Mar 2012 18:42:52 +0100

python-mongoengine (0.5.2-1) unstable; urgency=low

  * New upstream release.
  * Add libjs-underscore dependency to python-mongoengine-doc binary package.
  * Add DM-Upload-Allowed control field.

 -- Janos Guljas <janos@resenje.org>  Fri, 18 Nov 2011 16:28:17 +0100

python-mongoengine (0.5-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.9.2.
  * Remove version from build dependencies python-all and python-pymongo.
  * Remove XS-Python-Version and XB-Python-Version.
  * Make package synopsis compliant with Debian Reference section 6.2.2.
  * Override dh_installchangelogs to install upstream changelog.
  * Remove debian/patches/fix-pymongo-1.10-compatibility.
  * Add debian/source/local-options.
  * Fix debian/copyright DEP5 errors.
  * Add debian/python-mongoengine-doc.links for jquery libraries.

 -- Janos Guljas <janos@resenje.org>  Tue, 13 Sep 2011 03:50:05 +0200

python-mongoengine (0.4-2) unstable; urgency=low

  * Move tests to mongoengine module. (Closes: #620397)
  * Add debian/patches/fix-pymongo-1.10-compatibility
    - PyMongo changed map_reduce method in Collection class to require third
      parameter 'out' for output collection name. This patch is fixing this
      without breaking compatibility with earlier versions of PyMongo.

 -- Janos Guljas <janos@resenje.org>  Sat, 02 Apr 2011 17:48:34 +0200

python-mongoengine (0.4-1) unstable; urgency=low

  * Initial release. (Closes: #609850)

 -- Janos Guljas <janos@resenje.org>  Sun, 23 Jan 2011 23:19:39 +0100
